#!/bin/sh

set -e
set -u

VER=`dpkg-parsechangelog|grep ^Version:|cut -f2 -d' '`
SVN_REV=`echo $VER | perl -ne 'print $1 if /~svn\+([[:digit:]]+)-/'`
REV_FILE=src/revisioninfo.h
UPDATER=update-revision-info.sh

if [ -n "$SVN_REV" ]; then
    echo Detected Snapshot build, r$SVN_REV

    [ -e $UPDATER.debian-bak ] || mv $UPDATER $UPDATER.debian-bak
    sed -e \
        '/^if test/,$ { s/.\+/echo "$0 not used in the Debian build"/; p; Q}' \
        < $UPDATER.debian-bak > $UPDATER
    chmod --reference=$UPDATER.debian-bak $UPDATER
    echo "#define FR_VERSION_SVN $SVN_REV" > $REV_FILE
    echo "#undef FR_GIT_HASH" >> $REV_FILE
fi
