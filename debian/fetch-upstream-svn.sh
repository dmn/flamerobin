#!/bin/sh

set -e
set -u

SVN=https://flamerobin.svn.sourceforge.net/svnroot/flamerobin/trunk/flamerobin

if [ -z "${2:-}" -o -n "${3:-}" ];
then
    echo Syntax: $0 version revision
    exit 1
fi

VER=$1
REV=$2

DATE=`svn info -r $REV $SVN | awk '/Last Changed Date:/ { print $4 }' | sed -e 's/-//g'`

DIR="flamerobin-$VER~svn${DATE}r$REV"

svn export -r $REV $SVN $DIR

tar c -s -v $DIR \
    | gzip -9 -n \
    > ../flamerobin_$VER~svn${DATE}r${REV}.orig.tar.gz

echo ../flamerobin_$VER~svn${DATE}r${REV}.orig.tar.gz ready.

rm -r $DIR
