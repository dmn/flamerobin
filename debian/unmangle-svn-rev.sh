#!/bin/sh

set -e
set -u

VER=`dpkg-parsechangelog|grep ^Version:|cut -f2 -d' '`
SVN_REV=`echo $VER | perl -ne 'print $1 if /~svn[[:digit:]]+r([[:digit:]]+)-/'`
UPDATER=update-revision-info.sh
REV_FILE=src/revisioninfo.h

if [ -n "$SVN_REV" ]; then
    [ ! -e "$UPDATER.debian-bak" ] || mv -f "$UPDATER.debian-bak" "$UPDATER"
    rm -f $REV_FILE
fi
